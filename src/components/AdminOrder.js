import Table from 'react-bootstrap/Table';
import { useEffect, useState } from 'react';
import { Col, Row, Container} from 'react-bootstrap';

export default function AdminOrders({ ordersData, fetchData }) {

	const [orders, setOrders] = useState([]);

	useEffect(() => {
		setOrders(ordersData);
	},[ordersData]);

	return (
    <div className="container mt-4">
      <h1>Orders</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Product</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Total Amount</th>
          </tr>
        </thead>
        <tbody>
          {orders.map((order) => (
            <>
              {order.products.map((product) => (
                <tr key={product.productId}>
                  <td>{order._id}</td> {/* Assuming you want to display the order ID for each product */}
                  <td>{product.productId}</td>
                  <td>{product.quantity}</td>
                  <td>{product.price}</td>
                  <td>{order.totalAmount}</td> {/* Assuming the totalAmount belongs to the entire order */}
                </tr>
              ))}
            </>
          ))}
        </tbody>
      </table>
    </div>
  );
} 	