import { Button, Row, Col } from 'react-bootstrap';


export default function Banner() {
	
	return (
			<Row>
				<Col className="p-5 text-center">
					<h1>E-Commerce App</h1>
					<p>Opportunities for everyone, everywhere.</p>
					<Button variant="primary">Buy Products Now!</Button>
				</Col>
			</Row>
			
		) 
};

