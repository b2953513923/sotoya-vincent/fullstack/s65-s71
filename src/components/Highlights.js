import {Row, Col, Card, } from 'react-bootstrap';


export default function Highlights() {

	return (

		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>Unwind and Refresh</Card.Title>
				        <Card.Text>
				          Treat yourself to a spa-like experience at home with our premium range of toiletries. From invigorating shower gels to nourishing body lotions, elevate your self-care routine to new heights of indulgence. 
				        </Card.Text>
				      </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>Glowing Skin, Inside Out</Card.Title>
				        <Card.Text>
				          	Discover the secret to radiant skin with our handpicked selection of skincare products. Embrace the power of natural ingredients and gentle formulas for a glowing complexion that will leave you feeling confident and beautiful
				        </Card.Text>
				      </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>Indulge in Blissful Baths</Card.Title>
				        <Card.Text>
				          Dive into a world of relaxation and rejuvenation with our luxurious bath essentials. Experience the perfect blend of fragrant soaps, silky bath oils, and fluffy towels - your well-deserved pampering awaits! 
				        </Card.Text>
				      </Card.Body>
				</Card>
			</Col>
		
		</Row>

	)

};