import { useState, useEffect } from 'react';

import {Row, Col, Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({ productProp }) {
  const { _id, name, description, price } = productProp;

  return (
    <Col xs={12} sm={6} md={4}> {/* Adjusted the column size for responsiveness */}
      <Card id="productComponent1 p-3">
        <Card.Body id="cards">
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>{price}</Card.Text>
          <Button as={Link} to={`/products/${_id}`} variant="primary">
            View Details
          </Button>
        </Card.Body>
      </Card>
    </Col>
  );
  
};
