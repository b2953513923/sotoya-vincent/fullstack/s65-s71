import React, { useState } from 'react';

export default function UpdateProfile(){
  const [fullName, setFullName] = useState('');
  const [message, setMessage] = useState('');
  const [ newProfile ] = useState('');
  
  const handleUpdateProfile = async () => {
    setMessage('');

    const profileData = {
      fullName,
    };

    try {
      const token = localStorage.getItem('token'); 
      const response = await fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(profileData)
      });

      if (response.ok) {
        setMessage('Profile updated successfully');
        setFullName('');
      } else {
        const errorData = await response.json();
        setMessage(errorData.message);
      }
    } catch (error) {
      setMessage('An error occurred. Please try again.');
      console.error(error);
    }
  };

  return (
    <div className="container mt-4 bg-white p-5 rounded my-5">
      <h2>Update Profile</h2>
      {message && <div className="alert alert-danger">{message}</div>}
      <div className="form-group">
        <label htmlFor="fullName">Full Name</label>
        <input
          type="text"
          className="form-control"
          id="fullName"
          value={fullName}
          onChange={(e) => setFullName(e.target.value)}
        />
      </div>
      <button type="submit" className="btn btn-primary mt-3" onClick={handleUpdateProfile}>
        Update Profile
      </button>
    </div>
  );
};

