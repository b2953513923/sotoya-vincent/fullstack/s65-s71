import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
// import ProductSearch from './ProductSearch';
// import SearchByPrice from './SearchByPrice'


export default function UserOrder({ordersData}) {

  const [orders, setOrders] = useState([])
  const [ products ] = useState([])

  useEffect(() => {
    const ordersArr = ordersData.map(order => {

      //only render the active products since the route used is /all from Product.js page
      if(order.isActive === true) {
        return (
          <ProductCard orderProp={order} key={order._id}/>
          )
      } else {
        return null;
      }
    })

    //set the products state to the result of our map function, to bring our returned product component outside of the scope of our useEffect where our return statement below can see.
    setOrders(ordersArr)

  }, [ordersData])

  return(
    <>
     {/* <ProductSearch/>*/}
     {/* <SearchByPrice/>*/}
      { products }
      { orders }
    </>
  )
}

