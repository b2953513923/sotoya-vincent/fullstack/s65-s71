import Banner from '../components/Banner';
// import CourseCard from '../components/CourseCard'
import Highlights from '../components/Highlights';
import FeaturedProducts from '../components/FeaturedProducts'



export default function Home() {

	return (
		<>
			<Banner/>
			<FeaturedProducts/>
			<Highlights/>
			{/*<CourseCard/>*/}
		</>
	)

};