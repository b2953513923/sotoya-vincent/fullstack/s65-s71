import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function ProductView() {

	// The "useParams" hook allows us to retrieve the productId passed via the URL
	const {productId} = useParams();

	const { user } = useContext(UserContext);

	// an object with methods to redirect the user.
	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);

	useEffect(()=>{
		console.log(productId)

		// a fetch request that will retrieve the details of a specific product
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId])

	const handleQuantityChange = (event) => {
    const newQuantity = parseInt(event.target.value);
    setQuantity(newQuantity);
  };

	const purchase = (productId) => {

		fetch(`${ process.env.REACT_APP_API_URL }/orders/purchase`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data){
				Swal.fire({
					title: "Successfully ordered",
					icon: "success",
					text: "You have Successfully ordered this product."
				})

				// Allow us to navigate the user back to the product page programmaticlly insted of using component.
				navigate("/products")
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}

		});


	}

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<div className="d-flex justify-content-center align-items-center">
				               <span className="me-2">Quantity:</span>
				               <input
				                 type="number"
				                 min="1"
				                 value={quantity}
				                 onChange={handleQuantityChange}
				               />
				             </div>
							{user.id !== null ?
								<Button variant="primary" onClick={() => purchase(productId)}>Checkout</Button>
							:
								<Button as={Link} to="/login" variant="danger">Log in to Order</Button>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
