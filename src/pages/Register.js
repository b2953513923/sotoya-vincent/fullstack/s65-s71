import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Register() {

	const { user } = useContext(UserContext);

	// State hooks to store the values of the register form input fields.
	const [fullName, setFullName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	// State to determine whether the submit button is enabled or not.
	const [ isActive, setIsActive ] = useState(false);

	// check if the values are successfully binded
	console.log(fullName);
	console.log(email);
	console.log(password);
	console.log(confirmPassword);


	function registerUser(e) {

		// Prevents the page redirection via form submission
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json" 
			},
			body: JSON.stringify({
				fullName: fullName,
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data) {

				setFullName("");
				setEmail("");
				setPassword("");
				setConfirmPassword("");

				alert("Thank you for registering!")

			} else {

				alert("Please try again later.")
			}
		})

	}




	/*
		This side effect is to validate the states whether each state is empty or string or not, if password is correct and if mobileNo has 11 digits.
	*/
	useEffect(() => {

		if((fullName !== "" && email !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword)) {

			setIsActive(true)
		} else {

			setIsActive(false)
		}

	}, [fullName, email, password, confirmPassword])



	return (

		(user.id !== null) ?
			<Navigate to="/products" />
			:
			<Form onSubmit={e => registerUser(e)} className="register-form mt-5">
			      <h1>Register</h1>

			      <Form.Group className="mb-3" controlId="Full Name">
			        <Form.Label>Full Name</Form.Label>
			        <Form.Control 
			          type="text" 
			          placeholder="Enter First Name" 
			          required 
			          value={fullName}
			          onChange={e => {setFullName(e.target.value)}}
			        />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="Email address">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			          type="email" 
			          placeholder="name@example.com" 
			          required
			          value={email}
			          onChange={e => {setEmail(e.target.value)}}
			        />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="Password1">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			          type="password" 
			          placeholder="Enter Password" 
			          required 
			          value={password}
			          onChange={e => {setPassword(e.target.value)}}
			        />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="Password2">
			        <Form.Label>Confirm Password</Form.Label>
			        <Form.Control 
			          type="password" 
			          placeholder="Confirm Password" 
			          required 
			          value={confirmPassword}
			          onChange={e => {setConfirmPassword(e.target.value)}}
			        />
			      </Form.Group>

			      {
			        isActive
			          ? <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
			          : <Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
			      }

			    </Form>

		

	)

};