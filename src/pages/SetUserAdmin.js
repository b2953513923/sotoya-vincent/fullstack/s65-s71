import React, { useState } from 'react';

export default function AdminPanel() {
  const [userId, setUserId] = useState('');
  const [isAdmin, setIsAdmin] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [successMessage, setSuccessMessage] = useState('');

  const handleUserIdChange = (event) => {
    setUserId(event.target.value);
  };

  const handleCheckboxChange = (event) => {
    setIsAdmin(event.target.checked);
  };

  const handleUpdateAdminStatus = () => {
    setErrorMessage('');
    setSuccessMessage('');

    // Assuming you have stored the JWT token in a variable called 'jwtToken'
    // const jwtToken = 'YOUR_JWT_TOKEN';

    // Make the request to the backend API using the Fetch API
    fetch(`${process.env.REACT_APP_API_URL}/users/updateAdmin`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${ localStorage.getItem('token') }`
      },
      body: JSON.stringify({ userId: userId })
    })
    .then(response => {
      if (response.ok) {
        setSuccessMessage('User admin status updated successfully!');
      } else {
        setErrorMessage('Failed to update user admin status.');
      }
    })
    .catch(error => {
      setErrorMessage('An error occurred while updating user admin status.');
    });
  };

  return (
    <div className="updateUserAdmin-container mt-5">
      <h2>Admin Panel</h2>
      <div className="form-group">
        <label>User ID:</label>
        <input
          type="text"
          className="form-control"
          value={userId}
          onChange={handleUserIdChange}
        />
      </div>
      <div className="form-group">
        <label>
          <input
            type="checkbox"
            checked={isAdmin}
            onChange={handleCheckboxChange}
          />
          Set as Admin
        </label>
      </div>
      <button
        type="button"
        className="updateUser btn btn-primary"
        onClick={handleUpdateAdminStatus}
      >
        Update Admin Status
      </button>
      {errorMessage && <div className="alert alert-danger mt-3">{errorMessage}</div>}
      {successMessage && <div className="alert alert-success mt-3">{successMessage}</div>}
    </div>
  );
};

